# Alacritty Setup
[Alacritty](https://github.com/alacritty/alacritty) is the fastest terminal emulator in existence. Using the GPU for rendering enables optimizations that simply aren't possible without it. Alacritty currently supports macOS, Linux, BSD, and Windows.

## Requirements

- [Go Mono Nerd Font](https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/Go-Mono) font.

## Configuration
The configuration location that I use is: `$HOME/.config/alacritty/alacritty.yml`

