#!/bin/sh

mkdir -p ~/.local/share/mc/skins/
cp dracula256.ini ~/.local/share/mc/skins/

mkdir -p ~/.config/mc/
cp ini ~/.config/mc/ini
