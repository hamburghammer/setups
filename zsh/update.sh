#!/bin/sh

# Create porfiles if needed
touch $HOME/.profile
touch $HOME/.alias_profile
touch $HOME/.zsh_profile

cp ./.zshrc $HOME/.zshrc
cp ./starship.toml $HOME/.config/
