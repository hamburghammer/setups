# ZSH
The zsh config...

## Requires

- zsh
- [starship theme](https://starship.rs)
- The Terminal has to use a Nerd Font.

## Installation

1. Install zsh
2. Install starship
3. Run the `update.sh` script

## Configuration
The `.zshrc` will be copied to `$HOME/.zshrc` the `update.sh` script will also create a `$HOME/.profile` file if not available for the env variables and `$HOME/.alias_porfile` for the custom alias config.

