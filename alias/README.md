# Alias
This direcory holds alias configuration files.

To activate these aliases source the one you want in your shell like `$HOME/.alias/standard_alias`

## Requirements
There are some aliases for programms that are not usually installed. If you want to use theme simply install theme. For more information take a look into the specific files!

## Configuration
The configuration destionation is `$HOME/.alias/`
