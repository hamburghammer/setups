# My manjaro sway installation/configuration
Execute the `install.sh` script from this folder.
Everything inside this directory is only for initial installation after it use the individual directories to update the configs.

*WIP AND NOT TESTED!*

## fzf
For autocompletion fzf can be used. 

`ctrl+t` opens a prompt to search for a file.

`ctrl+r` can be used to search interactivly through the command history.
