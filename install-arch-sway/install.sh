#!/bin/bash
local PWD=$(command pwd)

# ZSH
command echo "Installing zsh config..."
command cd ../zsh/
command echo "Installing zsh and dependencies..."
command yay -S --needed - < dependencies_arch.txt
command echo "Copying the config files"
./update.sh
command cd $PWD
command echo "#########################################"

# Alias
command echo "Installing aliases..."
command cd ../alias/
command echo "Installing dependencies..."
command yay -S --needed - < dependencies_arch_better_standard.txt
command yay -S --needed - < dependencies_arch_unsorted.txt
command echo "Copying the config files"
./update.sh
command cd $PWD
command echo "#########################################"

# sway
command echo "Installing sway..."
command cd ../sway/
command echo "Installing dependencies..."
command yay -S --needed - < dependencies_arch.txt
command echo "Copying the config files"
./update.sh
command cd $PWD
command echo "#########################################"

# Alacritty
command echo "Installing alacritty..."
command cd ../alacritty/
command echo "Installing dependencies..."
command yay -S --needed - < dependencies_arch.txt
command echo "Copying the config files"
./update.sh
command cd $PWD
command echo "#########################################"

# Programs
command echo "Installing extra programs..."
command yay -S --needed - < programs.txt
command echo "Copying the config files"
./update.sh
command cd $PWD
command echo "#########################################"
