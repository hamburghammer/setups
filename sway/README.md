# Sway
Meine `sway` configuration.

## Requirements
- light
- pamixer
- pulseaudio
- pavucontrol
- wofi
- foot
- sway
- swaylock
- swayidle
- swaybg
- xorg-server-xwayland
- mako
- upower
- networkmanager
- waybar

// AUR
- wob // https://github.com/francma/wob import gpg key
- grimshot // https://github.com/swaywm/sway/blob/master/contrib/grimshot.1 manual

## Installation und Updates
Das `update.sh` Script kümmert sich um beides.
