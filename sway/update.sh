#!/bin/sh

mkdir -p $HOME/.config/sway/
touch $HOME/.config/sway/custom
cp config $HOME/.config/sway/
cp status.sh $HOME/.config/sway/
